﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Mime;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.Win32;

namespace Temper2
{
    public class UserPreferences
    {
        public static bool Autostart
        {
            get => (Get<string>(Application.ProductName, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run") != null) ? true : false;
            set
            {
                if (value) 
                    Set(Application.ProductName, Application.ExecutablePath, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");
                else
                    UserPreferences.Delete(Application.ProductName, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");
            }
        }

        public static string TemperPath
        {
            get => Get<string>("TemperPath");
            set => Set("TemperPath", value);
        }

        public static bool AutoOpenDir
        {
            get => Get<bool>("AutoOpenDir");
            set => Set("AutoOpenDir", value);
        }

        public static bool DefaultOpenDir
        {
            get => Get<bool>("DefaultOpenDir");
            set => Set("DefaultOpenDir", value);
        }

        private static string RegistrySubkeyPath
        {
            get
            {
                FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location);
                return "SOFTWARE\\" + versionInfo.CompanyName + "\\" + Application.ProductName;
            }
        }

        private static void Set<T>(string name, T value, string registryKeyPath)
        {
            try
            {
                RegistryKey userPreferencesKey = Registry.CurrentUser.CreateSubKey(registryKeyPath);
                userPreferencesKey.SetValue(name, value.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static T Get<T>(string name, string registryKeyPath)
        {
            try
            {
                RegistryKey userPreferencesKey = Registry.CurrentUser.OpenSubKey(registryKeyPath, false);
                string genericValue = userPreferencesKey.GetValue(name).ToString();
                T value = (T) Convert.ChangeType(genericValue, typeof(T));
                return value;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return default(T);
            }
        }

        private static void Set<T>(string name, T value)
        {
            Set(name, value, RegistrySubkeyPath);
        }

        private static T Get<T>(string name)
        {
            return Get<T>(name, RegistrySubkeyPath);
        }

        public static void Delete(string name, string registryKeyPath)
        {
            try
            {
                RegistryKey userPreferencesKey = Registry.CurrentUser.OpenSubKey(registryKeyPath, true);
                userPreferencesKey.DeleteValue(name, false);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void SetDefaults()
        {
            UserPreferences.Autostart = true;
            UserPreferences.TemperPath = Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.Personal)).FullName + @"\Temper";
            UserPreferences.AutoOpenDir = true;
            UserPreferences.DefaultOpenDir = true;
        }
    }
}