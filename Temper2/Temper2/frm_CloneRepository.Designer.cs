﻿namespace Temper2
{
    partial class frm_CloneRepository
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_clone = new System.Windows.Forms.Button();
            this.cbo_open = new System.Windows.Forms.CheckBox();
            this.txt_repositoryUrl = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_clone
            // 
            this.btn_clone.Location = new System.Drawing.Point(12, 64);
            this.btn_clone.Name = "btn_clone";
            this.btn_clone.Size = new System.Drawing.Size(274, 37);
            this.btn_clone.TabIndex = 2;
            this.btn_clone.Text = "Clone";
            this.btn_clone.UseVisualStyleBackColor = true;
            this.btn_clone.Click += new System.EventHandler(this.Btn_clone_Click);
            // 
            // cbo_open
            // 
            this.cbo_open.AutoSize = true;
            this.cbo_open.Checked = true;
            this.cbo_open.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbo_open.Location = new System.Drawing.Point(15, 37);
            this.cbo_open.Name = "cbo_open";
            this.cbo_open.Size = new System.Drawing.Size(147, 21);
            this.cbo_open.TabIndex = 1;
            this.cbo_open.Text = "Automatisch öfnen";
            this.cbo_open.UseVisualStyleBackColor = true;
            // 
            // txt_repositoryUrl
            // 
            this.txt_repositoryUrl.Location = new System.Drawing.Point(126, 9);
            this.txt_repositoryUrl.Name = "txt_repositoryUrl";
            this.txt_repositoryUrl.Size = new System.Drawing.Size(160, 22);
            this.txt_repositoryUrl.TabIndex = 0;
            this.txt_repositoryUrl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Txt_repositoryUrl_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Repository URL";
            // 
            // frm_CloneRepository
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(298, 113);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_repositoryUrl);
            this.Controls.Add(this.cbo_open);
            this.Controls.Add(this.btn_clone);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frm_CloneRepository";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Clone Git Repository";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_clone;
        private System.Windows.Forms.CheckBox cbo_open;
        private System.Windows.Forms.TextBox txt_repositoryUrl;
        private System.Windows.Forms.Label label1;
    }
}