﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Temper2
{
    public partial class frm_Settings : Form
    {
        public frm_Settings()
        {
            InitializeComponent();
        }

        private void frm_Settings_Load(object sender, EventArgs e)
        {
            this.LoadSettings();
            lbl_version.Text = "Version: " + Assembly.GetExecutingAssembly().GetName().Version;
        }

        private void btn_browse_Click(object sender, EventArgs e)
        {
            if (fbd_browse.ShowDialog() == DialogResult.OK)
            {
                txt_path.Text = fbd_browse.SelectedPath;
            }
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            this.SaveSettings();
            this.Close();
        }

        private void btn_apply_Click(object sender, EventArgs e)
        {
            this.SaveSettings();
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadSettings()
        {
            cbo_autostart.Checked = UserPreferences.Autostart;
            txt_path.Text = UserPreferences.TemperPath;
            cbo_autoOpenDir.Checked = UserPreferences.AutoOpenDir;
            cbo_defaultOpenDir.Checked = UserPreferences.DefaultOpenDir;
        }

        private void SaveSettings()
        {
            UserPreferences.Autostart = cbo_autostart.Checked;
            UserPreferences.TemperPath = txt_path.Text;
            UserPreferences.AutoOpenDir = cbo_autoOpenDir.Checked;
            UserPreferences.DefaultOpenDir = cbo_defaultOpenDir.Checked;
        }

        private void btn_clean_Click(object sender, EventArgs e)
        {
            try
            {
                Clean();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, Application.ProductName);
            }
        }

        private void Clean()
        {
            TemperDirectory temperDirectory = new TemperDirectory();
            int emptyTempsCount = 0;

            try
            {
                emptyTempsCount = temperDirectory.GetEmptyTemps().Count;
            }
            catch (Exception)
            {
                throw;
            }


            if (emptyTempsCount > 0)
            {
                DialogResult result = MessageBox.Show("Möchten Sie wirklich " + emptyTempsCount + " löschen?", Application.ProductName, MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    try
                    {
                        temperDirectory.DeleteEmptyTemps();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            else
            {
                throw new Exception("Keine zu löschenden Temps vorhanden");
            }
        }

        private void btn_defaults_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Möchten Sie wirklich die Standardeinstellungen wiederherstellen?", Application.ProductName, MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                UserPreferences.SetDefaults();
                LoadSettings();
            }
        }
    }
}
