﻿using System;
using System.Windows.Forms;

namespace Temper2
{
    public partial class frm_Create : Form
    {
        public frm_Create()
        {
            InitializeComponent();
        }

        private void frm_Create_Load(object sender, EventArgs e)
        {
            cbo_open.Checked = UserPreferences.DefaultOpenDir;
        }

        private void txt_name_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                try
                {
                    this.CreateTemp();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
            }
        }

        private void cbo_open_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    this.CreateTemp();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
            }
        }

        private void btn_create_Click(object sender, EventArgs e)
        {
            try
            {
                this.CreateTemp();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void CreateTemp()
        {
            string name = txt_name.Text.Trim();
            TemperDirectory temperDirectory = new TemperDirectory();

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new Exception("Kein Name angegeben");
            }

            Temp temp = temperDirectory.CreateTemp(name);

            temp.CreateDirectory();

            if (cbo_open.CheckState == CheckState.Checked)
            {
                temp.OpenDirectory();
            }

            this.Close();
        }
    }
}
