﻿namespace Temper2
{
    partial class frm_Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Settings));
            this.lbl_path = new System.Windows.Forms.Label();
            this.txt_path = new System.Windows.Forms.TextBox();
            this.btn_browse = new System.Windows.Forms.Button();
            this.btn_ok = new System.Windows.Forms.Button();
            this.btn_apply = new System.Windows.Forms.Button();
            this.fbd_browse = new System.Windows.Forms.FolderBrowserDialog();
            this.lbl_version = new System.Windows.Forms.Label();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.lbl_autoOpenDir = new System.Windows.Forms.Label();
            this.cbo_autoOpenDir = new System.Windows.Forms.CheckBox();
            this.cbo_defaultOpenDir = new System.Windows.Forms.CheckBox();
            this.lbl_autostart = new System.Windows.Forms.Label();
            this.cbo_autostart = new System.Windows.Forms.CheckBox();
            this.btn_clean = new System.Windows.Forms.Button();
            this.lbl_clean = new System.Windows.Forms.Label();
            this.btn_defaults = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_path
            // 
            this.lbl_path.AutoSize = true;
            this.lbl_path.Location = new System.Drawing.Point(14, 15);
            this.lbl_path.Name = "lbl_path";
            this.lbl_path.Size = new System.Drawing.Size(37, 17);
            this.lbl_path.TabIndex = 0;
            this.lbl_path.Text = "Pfad";
            // 
            // txt_path
            // 
            this.txt_path.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_path.Location = new System.Drawing.Point(156, 12);
            this.txt_path.Name = "txt_path";
            this.txt_path.Size = new System.Drawing.Size(316, 22);
            this.txt_path.TabIndex = 1;
            // 
            // btn_browse
            // 
            this.btn_browse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_browse.Location = new System.Drawing.Point(478, 12);
            this.btn_browse.Name = "btn_browse";
            this.btn_browse.Size = new System.Drawing.Size(114, 23);
            this.btn_browse.TabIndex = 2;
            this.btn_browse.Text = "Durchsuchen";
            this.btn_browse.UseVisualStyleBackColor = true;
            this.btn_browse.Click += new System.EventHandler(this.btn_browse_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_ok.Location = new System.Drawing.Point(12, 184);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(100, 23);
            this.btn_ok.TabIndex = 98;
            this.btn_ok.Text = "OK";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_apply
            // 
            this.btn_apply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_apply.Location = new System.Drawing.Point(118, 184);
            this.btn_apply.Name = "btn_apply";
            this.btn_apply.Size = new System.Drawing.Size(100, 23);
            this.btn_apply.TabIndex = 99;
            this.btn_apply.Text = "Übernehmen";
            this.btn_apply.UseVisualStyleBackColor = true;
            this.btn_apply.Click += new System.EventHandler(this.btn_apply_Click);
            // 
            // fbd_browse
            // 
            this.fbd_browse.RootFolder = System.Environment.SpecialFolder.UserProfile;
            // 
            // lbl_version
            // 
            this.lbl_version.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_version.AutoSize = true;
            this.lbl_version.Location = new System.Drawing.Point(461, 186);
            this.lbl_version.Name = "lbl_version";
            this.lbl_version.Size = new System.Drawing.Size(131, 17);
            this.lbl_version.TabIndex = 9;
            this.lbl_version.Text = "Version: unbekannt";
            this.lbl_version.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // btn_cancel
            // 
            this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_cancel.Location = new System.Drawing.Point(224, 184);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(100, 23);
            this.btn_cancel.TabIndex = 100;
            this.btn_cancel.Text = "Abbrechen";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // lbl_autoOpenDir
            // 
            this.lbl_autoOpenDir.AutoSize = true;
            this.lbl_autoOpenDir.Location = new System.Drawing.Point(14, 43);
            this.lbl_autoOpenDir.Name = "lbl_autoOpenDir";
            this.lbl_autoOpenDir.Size = new System.Drawing.Size(133, 17);
            this.lbl_autoOpenDir.TabIndex = 3;
            this.lbl_autoOpenDir.Text = "Ordner auto. öffnen";
            // 
            // cbo_autoOpenDir
            // 
            this.cbo_autoOpenDir.AutoSize = true;
            this.cbo_autoOpenDir.Location = new System.Drawing.Point(156, 62);
            this.cbo_autoOpenDir.Name = "cbo_autoOpenDir";
            this.cbo_autoOpenDir.Size = new System.Drawing.Size(308, 21);
            this.cbo_autoOpenDir.TabIndex = 4;
            this.cbo_autoOpenDir.Text = "Ordner beim automatischen Erstellen öffnen";
            this.cbo_autoOpenDir.UseVisualStyleBackColor = true;
            // 
            // cbo_defaultOpenDir
            // 
            this.cbo_defaultOpenDir.AutoSize = true;
            this.cbo_defaultOpenDir.Location = new System.Drawing.Point(156, 89);
            this.cbo_defaultOpenDir.Name = "cbo_defaultOpenDir";
            this.cbo_defaultOpenDir.Size = new System.Drawing.Size(281, 21);
            this.cbo_defaultOpenDir.TabIndex = 5;
            this.cbo_defaultOpenDir.Text = "Standardwert für das manuelle erstellen";
            this.cbo_defaultOpenDir.UseVisualStyleBackColor = true;
            // 
            // lbl_autostart
            // 
            this.lbl_autostart.AutoSize = true;
            this.lbl_autostart.Location = new System.Drawing.Point(14, 115);
            this.lbl_autostart.Name = "lbl_autostart";
            this.lbl_autostart.Size = new System.Drawing.Size(65, 17);
            this.lbl_autostart.TabIndex = 10;
            this.lbl_autostart.Text = "Autostart";
            // 
            // cbo_autostart
            // 
            this.cbo_autostart.AutoSize = true;
            this.cbo_autostart.Location = new System.Drawing.Point(156, 116);
            this.cbo_autostart.Name = "cbo_autostart";
            this.cbo_autostart.Size = new System.Drawing.Size(253, 21);
            this.cbo_autostart.TabIndex = 6;
            this.cbo_autostart.Text = "Bei Anmeldung automatisch starten";
            this.cbo_autostart.UseVisualStyleBackColor = true;
            // 
            // btn_clean
            // 
            this.btn_clean.Location = new System.Drawing.Point(148, 143);
            this.btn_clean.Name = "btn_clean";
            this.btn_clean.Size = new System.Drawing.Size(100, 23);
            this.btn_clean.TabIndex = 8;
            this.btn_clean.Text = "Bereinigen";
            this.btn_clean.UseVisualStyleBackColor = true;
            this.btn_clean.Click += new System.EventHandler(this.btn_clean_Click);
            // 
            // lbl_clean
            // 
            this.lbl_clean.AutoSize = true;
            this.lbl_clean.Location = new System.Drawing.Point(14, 146);
            this.lbl_clean.Name = "lbl_clean";
            this.lbl_clean.Size = new System.Drawing.Size(128, 17);
            this.lbl_clean.TabIndex = 7;
            this.lbl_clean.Text = "Temper bereinigen";
            // 
            // btn_defaults
            // 
            this.btn_defaults.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_defaults.Location = new System.Drawing.Point(330, 184);
            this.btn_defaults.Name = "btn_defaults";
            this.btn_defaults.Size = new System.Drawing.Size(100, 23);
            this.btn_defaults.TabIndex = 101;
            this.btn_defaults.Text = "Standard";
            this.btn_defaults.UseVisualStyleBackColor = true;
            this.btn_defaults.Click += new System.EventHandler(this.btn_defaults_Click);
            // 
            // frm_Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 219);
            this.Controls.Add(this.btn_defaults);
            this.Controls.Add(this.lbl_clean);
            this.Controls.Add(this.btn_clean);
            this.Controls.Add(this.cbo_autostart);
            this.Controls.Add(this.lbl_autostart);
            this.Controls.Add(this.cbo_defaultOpenDir);
            this.Controls.Add(this.cbo_autoOpenDir);
            this.Controls.Add(this.lbl_autoOpenDir);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.lbl_version);
            this.Controls.Add(this.btn_apply);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.btn_browse);
            this.Controls.Add(this.txt_path);
            this.Controls.Add(this.lbl_path);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frm_Settings";
            this.Text = "Einstellungen";
            this.Load += new System.EventHandler(this.frm_Settings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_path;
        private System.Windows.Forms.TextBox txt_path;
        private System.Windows.Forms.Button btn_browse;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.Button btn_apply;
        private System.Windows.Forms.FolderBrowserDialog fbd_browse;
        private System.Windows.Forms.Label lbl_version;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Label lbl_autoOpenDir;
        private System.Windows.Forms.CheckBox cbo_autoOpenDir;
        private System.Windows.Forms.CheckBox cbo_defaultOpenDir;
        private System.Windows.Forms.Label lbl_autostart;
        private System.Windows.Forms.CheckBox cbo_autostart;
        private System.Windows.Forms.Button btn_clean;
        private System.Windows.Forms.Label lbl_clean;
        private System.Windows.Forms.Button btn_defaults;
    }
}