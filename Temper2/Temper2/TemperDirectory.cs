﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Temper2
{
    public class TemperDirectory
    {
        public List<Temp> GetTemps()
        {
            List<Temp> temps = new List<Temp>();
            List<string> directories = null;

            try
            {
                directories = Directory.GetDirectories(UserPreferences.TemperPath).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            directories.ForEach(d => temps.Add(new Temp(System.IO.Path.GetFileName(d))));
            return temps;
        }

        public List<Temp> GetEmptyTemps()
        {
            List<Temp> temps = GetTemps();

            try
            {
                temps.RemoveAll(t => Directory.GetFileSystemEntries(t.GetAbsolutePath()).Length > 0);
            }
            catch (Exception)
            {
                throw;
            }

            return temps;
        }

        public void DeleteEmptyTemps()
        {
            List<Temp> emptyTemps = GetEmptyTemps();
            
            try
            {
                emptyTemps.ForEach(t => t.DeleteDirectory());
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] GetFilesInRoot()
        {
            return Directory.GetFiles(UserPreferences.TemperPath);
        }

        public void OpenDirectory()
        {
            try
            {
                Process.Start(UserPreferences.TemperPath);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Temp CreateTemp()
        {
            Temp temp = new Temp();
            return temp;
        }

        public Temp CreateTemp(string name)
        {
            Temp temp = new Temp(name);
            return temp;
        }
    }
}