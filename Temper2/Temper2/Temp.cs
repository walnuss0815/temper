﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Temper2.Properties;

namespace Temper2
{
    public class Temp
    {
        public string Name { get; }

        public Temp()
        {
            Name = GenerateName();
        }

        public Temp(string name)
        {
            Name = name;
        }

        public void CreateDirectory()
        {
            try
            {
                Directory.CreateDirectory(GetAbsolutePath());
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteDirectory()
        {
            try
            {
                Directory.Delete(GetAbsolutePath());
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void OpenDirectory()
        {
            try
            {
                Process.Start(GetAbsolutePath());
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DirectoryExists()
        {
            try
            {
                return Directory.Exists(GetAbsolutePath());
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] GetFileSystemEntries()
        {
            try
            {
                return Directory.GetFileSystemEntries(GetAbsolutePath());
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetAbsolutePath()
        {
            return UserPreferences.TemperPath + Path.DirectorySeparatorChar + Name;
        }

        private static string GenerateName()
        {
            return DateTime.Now.ToString("yyyyMMdd-HHmmssfff");
        }
    }
}