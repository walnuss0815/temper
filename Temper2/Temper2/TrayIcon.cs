﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Reflection;
using System.Diagnostics;

namespace Temper2
{
    public partial class TrayIcon
    {
        private static NotifyIcon nico;
        private static frm_Settings frmSettings;
        private static frm_CloneRepository frmCloneRepository;
        private static frm_Create frmCreate;

        public TrayIcon()
        {
            ContextMenu cmenue = new ContextMenu();
            nico = new NotifyIcon();

            frmSettings = new frm_Settings();
            frmCloneRepository = new frm_CloneRepository();
            frmCreate = new frm_Create();

            cmenue.MenuItems.Add(new MenuItem("&Neu", new System.EventHandler(openNewClick)));
            cmenue.MenuItems.Add(new MenuItem("&Clone Git-Repository", new System.EventHandler(openCloneRepository)));
            cmenue.MenuItems.Add(new MenuItem("&Ordner öffnen", new System.EventHandler(openDirClick)));
            cmenue.MenuItems.Add(new MenuItem("&Einstellungen", new System.EventHandler(openSettingsClick)));
            cmenue.MenuItems.Add(new MenuItem("&Schließen", new System.EventHandler(exitClick)));

            nico.Icon = Properties.Resources.temper2;
            nico.Text = "Temper";
            nico.Visible = true;
            nico.ContextMenu = cmenue;
            nico.DoubleClick += new EventHandler(newDirDoubleClick);
        }

        private void openNewClick(object sender, EventArgs e)
        {
            if (frmCreate.Visible)
            {
                frmCreate.Focus();
            }
            else
            {
                frmCreate = new frm_Create();
                frmCreate.Show();
            }
        }

        private void openCloneRepository(object sender, EventArgs e)
        {
            if (frmCloneRepository.Visible)
            {
                frmCloneRepository.Focus();
            }
            else
            {
                frmCloneRepository = new frm_CloneRepository();
                frmCloneRepository.Show();
            }
        }

        private static void openDirClick(Object sender, EventArgs e)
        {
            TemperDirectory temperDirectory = new TemperDirectory();
            try
            {
                temperDirectory.OpenDirectory();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private static void openSettingsClick(Object sender, EventArgs e)
        {
            if (frmSettings.Visible)
            {
                frmSettings.Focus();
            }
            else
            {
                frmSettings = new frm_Settings();
                frmSettings.Show();
            }  
        }

        private static void exitClick(Object sender, EventArgs e)
        {
            nico.Dispose();
            Application.Exit();
        }

        private static void newDirDoubleClick(Object sender, EventArgs e)
        {
            TemperDirectory temperDirectory = new TemperDirectory();
            Temp temp = temperDirectory.CreateTemp();

            try
            {
                temp.CreateDirectory();
                if(UserPreferences.AutoOpenDir == true)
                {
                    temp.OpenDirectory();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }
    }
}
